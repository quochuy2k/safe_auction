import rootReducer from './src/reducers/index'
import rootSaga from './src/sagas/index';
import createSagaMiddleware from 'redux-saga'

import { createStore, 
        combineReducers, 
        applyMiddleware } from 'redux'

const sagaMiddleware = createSagaMiddleware()
//const store = createStore(rootReducer,applyMiddleware(sagaMiddleware,logger))
const store = createStore(rootReducer,applyMiddleware(sagaMiddleware))
sagaMiddleware.run(rootSaga)

export default store;
