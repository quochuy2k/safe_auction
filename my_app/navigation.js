import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { View, Text, StyleSheet, Dimensions, StatusBar } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { createStackNavigator } from '@react-navigation/stack';
import {Icon} from 'react-native-elements'
import LoginContainer from './src/containers/LoginContainer'
import HomeContainer from './src/containers/HomeContainer'
import AuctionContainer from './src/containers/AuctionContainer'
const Stack = createStackNavigator();
const initialLayout = { width: Dimensions.get('window').width };

const renderScene = SceneMap({
    first: HomeContainer,
    second: AuctionContainer,
});

 function Home() {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first' },
        { key: 'second' },
    ]);
    const getTabBarIcon = (props) => {
        const {route} = props
    
          if (route.key === 'first') {
    
           return <Icon
           name='home-outline'
           type='ionicon'
           color='pink'
           size={25}
         /> 
          } else {
            return <Icon
            name='hammer-outline'
            type='ionicon'
            color='pink'
            size={25}
          />
    
          }
    }
    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: 'pink' }}
            style={{ backgroundColor: 'white' }}
            renderIcon={
                props => getTabBarIcon(props)
            }
        />
    );

    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            style={styles.container}
            renderTabBar={renderTabBar}
            tabBarPosition={'bottom'}
        />
    );
}
export default function Navigation() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login"
                screenOptions={{
                    headerShown: false
                }}
            >
                <Stack.Screen name="Login" component={LoginContainer}
                />
                <Stack.Screen name="Home" component={Home} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}
const styles = StyleSheet.create({
    container: {
        marginTop: StatusBar.currentHeight,
    },
    scene: {
        flex: 1,
    },
});

