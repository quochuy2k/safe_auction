import React from 'react'
import { View, ImageBackground, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import AuctionComponent from '../components/AuctionComponent'
import * as actions from '../actions/index'
import * as types from '../common/index'

class AuctionContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
        this.props.get_product()
    }
    componentDidUpdate(){
        // this.props.get_product()  
    }

    render() {
        return (
            <AuctionComponent {...this.props} />
        )
    }
}
const styles = StyleSheet.create({

})
const mapStateToProps = (state) => {
    return {
        product: state.productState,
        user:state.userState.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        get_product: () => {
            dispatch(actions.get_product())
        },
        update_product:(data)=>{
            dispatch(actions.update_product(data))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(AuctionContainer);