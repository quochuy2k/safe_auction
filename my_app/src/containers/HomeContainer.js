import React from 'react'
import { View, ImageBackground, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import HomeComponent from '../components/HomeComponent'
import * as actions from '../actions/index'

class HomeContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
       
    }
    componentDidMount() {
        console.log("user...",this.props.user.data);
      if (this.props.user) {
          this.props.get_product_by_author(this.props.user.data._id)
          this.props.get_product_by_customer(this.props.user.data._id)
      }
    }

    render() {
        return (
            <HomeComponent {...this.props} />
        )
    }
}
const mapStateToProps = (state) => {
    return {
        user:state.userState.user,
        product:state.productState
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        create_product:(data)=>{
            dispatch(actions.create_product(data))
        },
        get_product_by_author:(data)=>{
            dispatch(actions.get_product_by_author(data))
        },
        get_product_by_customer:(data)=>{
            dispatch(actions.get_product_by_customer(data))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);