import React from 'react'
import { View, ImageBackground } from 'react-native';
import { connect } from 'react-redux'
import * as actions from '../actions/index'
import { StyleSheet } from 'react-native'
import LoginComponent from '../components/LoginComponent'
class LoginContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        }

    }
    componentDidMount() {

    }
    render() {
        return (
            <LoginComponent {...this.props} />

        )
    }
}
const styles = StyleSheet.create({

})
const mapStateToProps = (state) => {
    return {
        user:state.userState.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        login:(data)=>{
            dispatch(actions.login(data))
        },
        register:(data)=>{
            dispatch(actions.register(data))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
