import React, { useEffect, useState } from "react";
import { View, StyleSheet, ScrollView, ActivityIndicator, Alert, Modal, Pressable, TextInput } from "react-native";
import { Icon, Text, Image } from 'react-native-elements'
import Textarea from 'react-native-textarea';
import ProductComponent from './ProductComponent'
const array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
export default function HomeComponent(props) {
    const [modalVisible, setModalVisible] = useState(false);
    const [nameProduct, setNameProduct] = useState('');
    const [priceProduct, setPriceProduct] = useState('');
    const [timelifeProduct, setTimelifeProduct] = useState('');
    const [desProduct, setDesProduct] = useState('')
    const submit_product = () => {
        let data = {
            name: nameProduct,
            price: priceProduct,
            description: desProduct,
            time_life: timelifeProduct,
            author: props.user.data._id
        }
        props.create_product(data)
        setModalVisible(!modalVisible)
    }
    return (
        <View style={styles.container}>
            <View style={styles.row}>
                <Icon
                    reverse
                    name='person-outline'
                    type='ionicon'
                    color='pink'
                    size={20}
                />
                <View style={styles.buttonUser}>
                    <Text>{props.user.data.full_name}</Text>
                </View>
                <View style={styles.icon}>
                    <Icon

                        reverse
                        name='add-outline'
                        type='ionicon'
                        color='pink'
                        size={20}
                        onPress={() => setModalVisible(!modalVisible)}
                    />
                </View>


            </View>
            <ProductComponent
                {...props}
                title='Sản Phẩm'
                listItem={props.product.product_author.data}
            />
            <ProductComponent
                {...props}
                title='Đã mua'
                listItem={props.product.product_customer.data}
            />

            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Bạn muốn bán</Text>
                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.TextInput}
                                placeholder="Tên sản phẩm"
                                placeholderTextColor="#003f5c"
                                secureTextEntry={false}
                                onChangeText={(nameProduct) => setNameProduct(nameProduct)}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.TextInput}
                                placeholder="Giá sản phẩm"
                                placeholderTextColor="#003f5c"
                                secureTextEntry={false}
                                onChangeText={(price) => setPriceProduct(price)}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.TextInput}
                                placeholder="Thời gian đấu giá"
                                placeholderTextColor="#003f5c"
                                secureTextEntry={false}
                                onChangeText={(time) => setTimelifeProduct(time)}
                            />
                        </View>
                        <View style={styles.DescriptionInputView}>
                            <Textarea
                                containerStyle={styles.textareaContainer}
                                style={styles.textarea}
                                // onChangeText={this.onChange}
                                maxLength={200}
                                placeholder={'Mô tả sản phẩm ...'}
                                underlineColorAndroid={'transparent'}
                                placeholderTextColor={'black'}
                                onChangeText={(des) => setDesProduct(des)}
                            />
                        </View>
                        <View style={styles.footerModal}>
                            <Pressable
                                style={[styles.button, styles.buttonClose]}
                                onPress={() => setModalVisible(!modalVisible)}
                            >
                                <Text style={styles.textStyle}>Cancel</Text>
                            </Pressable>
                            <Pressable
                                style={[styles.button, styles.buttonSubmit]}
                                onPress={() => submit_product()}
                            >
                                <Text style={styles.textStyle}>Submit</Text>
                            </Pressable>
                        </View>

                    </View>
                </View>
            </Modal>

        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scene: {
        flex: 1,
    },
    row: {
        flexDirection: "row",
        flexWrap: "wrap",
    },
    buttonUser: {
        marginTop: 7,
        width: '60%',
        height: 45,
        borderRadius: 25,
        borderWidth: 2,
        borderColor: 'pink',
        alignItems: 'center',
        paddingTop: 10

    },
    icon: {
        marginLeft: 20
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        paddingTop: 35,
        paddingRight: 15,
        paddingLeft: 15,
        alignItems: "center",
        shadowColor: "#000",
        width: '90%',
        height: '80%',
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonSubmit: {
        backgroundColor: "green",
        fontStyle: "italic",
        margin: 5
    },
    buttonClose: {
        backgroundColor: "red",
        fontStyle: "italic",
        margin: 5

    },
    textStyle: {
        color: "black",
        fontWeight: "bold",
        textAlign: "center",
        fontStyle: "italic",
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontSize: 24,
        fontStyle: "italic",
    },
    inputView: {
        backgroundColor: "#FFC0CB",
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 20,

    },
    TextInput: {
        height: 50,
        flex: 1,
        padding: 10,
        marginLeft: 20,
        fontStyle: "italic",

    },
    DescriptionInputView: {
        backgroundColor: "#FFC0CB",
        borderRadius: 30,
        width: "70%",
        height: 250,
        marginBottom: 20,
    },
    DescriptionInput: {
        height: 200,
        flex: 1,
        padding: 10,
        marginLeft: 20,
        fontStyle: "italic",


    },
    textareaContainer: {
        height: 180,
        padding: 5,
        backgroundColor: '#FFC0CB',
        borderRadius: 30,
    },
    textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        padding: 10,
        color: 'black',
        fontStyle: "italic",
    },
    footerModal: {
        flexDirection: "row",
        flexWrap: "wrap",
    }
});