import React, { useEffect, useState } from "react";
import { View, StyleSheet, ScrollView, ActivityIndicator } from "react-native";
import { Icon, Text, Image } from 'react-native-elements'
import image from '../common/imageCommon'
export default function ProductComponent(props) {
    let item = []
    if (props.listItem) {
        item = props.listItem.map((element, key) => {
            return (
                <View   key={key} style={styles.itemProduct}>
                   <Image
                  
                    source={image[element.avata]}
                    style={styles.image}
                    PlaceholderContent={<ActivityIndicator />}
                />  
                <Text>{element.name}</Text>
                </View>
               
            )
        })
    }
    return (

        <View>
            <View style={styles.product_left_but}>
                <Text>{props.title}</Text>
            </View>
            <View style={styles.product_left}>
                <ScrollView >
                    <View style={styles.scroll_view}>
                        {item}
                    </View>

                </ScrollView>
            </View>
        </View>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    itemProduct:{
        width: 100,
        alignItems:'center',
        margin: 5
    },
    scene: {
        flex: 1,
    },
    row: {
        flexDirection: "row",
        flexWrap: "wrap",
    },
    buttonUser: {
        marginTop: 7,
        width: '60%',
        height: 45,
        borderRadius: 25,
        borderWidth: 2,
        borderColor: 'pink',
        alignItems: 'center',
        paddingTop: 10

    },
    product_left_but: {
        width: 100,
        height: 40,
        backgroundColor: 'pink',
        borderTopRightRadius: 50,
        borderBottomRightRadius: 25,
        marginTop: 30,
        alignItems: 'center',
        paddingTop: 10
    },
    product_left: {
        width: '90%',
        height: 250,

        marginLeft: 20,
        alignItems: 'center',

    },
    scroll_view: {
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 20
    },
    image: {
        width: 100,
        height: 100
        
    }
});