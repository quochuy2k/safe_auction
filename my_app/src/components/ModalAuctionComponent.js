import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Animated, ActivityIndicator, Alert, Modal, Pressable, TextInput } from "react-native";
import { Icon, Text, Image, Button } from 'react-native-elements'
import Textarea from 'react-native-textarea';
import * as types from '../common/index'
import { io } from "socket.io-client";
import image from '../common/imageCommon'
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer'


export function UrgeWithPleasureComponent(props) {
    const [time, setTime] = useState(0)
    const [check, setCheck] = useState(true)
    useEffect(() => {
        props.socket.on("time", (times) => {
            var d = new Date();
            let a = times - (d.getSeconds() + d.getMinutes() * 60 + d.getHours() * 3600)
            if (a >= 0) {
                setTime(a)
            }
            else {
                setTime(0)
            }

        });
    });
    if (time != 0) {
        return (
            <CountdownCircleTimer
                isPlaying
                duration={time}
                size={70}
                colors={[
                    ['#004777', 0.4],
                    ['#F7B801', 0.4],
                    ['#A30000', 0.2],
                ]}
            >
                {({ remainingTime, animatedColor }) => {
                    if (remainingTime == 0 && check == true) {
                        props.socket.emit('result', props.item_id)
                        props.socket.on("reponse_result", (data_result) => {
                            if (data_result == props.socket.id) {
                                props.update_item()
                                alert("Bạn đã mua thành công");

                            }

                            props.close_modal()

                        });

                        setCheck(false)
                    }
                    return (
                        <Animated.Text style={{ color: animatedColor }}>
                            {remainingTime}
                        </Animated.Text>
                    )
                }}
            </CountdownCircleTimer>
        )
    }
    else {
        return (
            <View>

            </View>
        )
    }

}
export function StateAuction(props) {
    const [price, setPrice] = useState('')
    const [stateAu, setStateAu] = useState(false)
    if (!stateAu) {
        const test = () => {
            props.send_message(parseInt(price) + parseInt(props.keyNumber))
            setStateAu(!stateAu)
        }
        return (

            <View style={styles.inputView}>
                <TextInput
                    style={styles.TextInput}
                    placeholder="Giá."
                    placeholderTextColor="#003f5c"
                    onChangeText={(price) => setPrice(price)}
                />
                <Button
                    icon={
                        <Icon
                            name="hammer-outline"
                            size={26}
                            type='ionicon'
                            color="white"
                        />
                    }
                    onPress={() => { test() }}
                />
            </View>

        )

    }
    else {
        return (
            <View>

            </View>
        )
    }
}
export default class ModalAuction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stateAuction: false,
        }

    }
    componentDidMount() {
        this.socket = io(types.HOST)
        this.socket.on("connect", () => {

        });

    }
    componentDidUpdate() {
        if (this.props.modalVisible) {
            this.socket.emit('join', this.props.item._id)
        }

    }
    componentWillUnmount() {
        this.socket.disconnect()

    }
    render() {
        const send_message = (data) => {
            let datas = {
                price: data,
                product: this.props.item._id
            }
            this.socket.emit('price', datas)
        }
        const close_modal = () => {
            this.props.setModalVisible(!this.props.modalVisible)
            this.props.get_product()
        }



        return (
            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => {
                    this.props.setModalVisible(!this.props.modalVisible);
                }}
            >
                <View style={styles.centeredView}>

                    <View style={styles.modalView}>

                        <UrgeWithPleasureComponent
                            {...this.props}
                            socket={this.socket}
                            item_id={this.props.item._id}
                            close_modal={close_modal}
                        />

                        <Text style={styles.modalText}>{this.props.item.name}</Text>
                        <Image
                            source={image[this.props.item.avata]}
                            style={styles.image}
                            PlaceholderContent={<ActivityIndicator />}
                        />
                        <Text style={styles.textStyle}>Mô tả : {this.props.item.description}</Text>
                        <Text style={styles.textStyle}>Giá khởi điểm : {this.props.item.price} $</Text>
                        <StateAuction
                            {...this.props}
                            keyNumber={this.props.item.key}
                            stateAuction={this.state.stateAuction}
                            send_message={send_message}
                        />
                        <View style={styles.footerModal}>

                        </View>

                    </View>
                    {/* <Icon
                        reverse
                        name='close-outline'
                        type='ionicon'
                        color='pink'
                        size={20}
                        onPress={() => this.props.setModalVisible(!this.props.modalVisible)}
                    /> */}
                </View>

            </Modal>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scene: {
        flex: 1,
    },
    butAuc: {
        borderTopRightRadius: 15,
        borderBottomRightRadius: 15

    },
    row: {
        flexDirection: "row",
        flexWrap: "wrap",
    },
    buttonUser: {
        marginTop: 7,
        width: '60%',
        height: 45,
        borderRadius: 25,
        borderWidth: 2,
        borderColor: 'pink',
        alignItems: 'center',
        paddingTop: 10

    },
    icon: {
        marginLeft: 20
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        paddingTop: 35,
        paddingRight: 15,
        paddingLeft: 15,
        alignItems: "center",
        shadowColor: "#000",
        width: '90%',
        height: '70%',
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonSubmit: {
        backgroundColor: "green",
        fontStyle: "italic",
    },
    buttonClose: {
        backgroundColor: "red",
        fontStyle: "italic",
        margin: 5

    },
    textStyle: {
        color: "black",
        fontWeight: "normal",
        textAlign: "center",
        fontStyle: "italic",
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontSize: 24,
        fontStyle: "italic",
    },
    inputView: {
        marginTop: 15,
        backgroundColor: "#FFC0CB",
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 10,
        flexDirection: "row",
        flexWrap: "wrap",

    },
    TextInput: {

        height: 50,
        flex: 1,
        padding: 10,
        marginLeft: 20,
        fontStyle: "italic",

    },
    DescriptionInputView: {
        backgroundColor: "#FFC0CB",
        borderRadius: 30,
        width: "70%",
        height: 250,
        marginBottom: 20,
    },
    DescriptionInput: {
        height: 200,
        flex: 1,
        padding: 10,
        marginLeft: 20,
        fontStyle: "italic",


    },
    textareaContainer: {
        height: 180,
        padding: 5,
        backgroundColor: '#FFC0CB',
        borderRadius: 30,
    },
    textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        padding: 10,
        color: 'black',
        fontStyle: "italic",
    },
    footerModal: {
        flexDirection: "row",
        flexWrap: "wrap",
    },
    image: {
        width: 100,
        height: 100,
        marginBottom: 20

    }
});