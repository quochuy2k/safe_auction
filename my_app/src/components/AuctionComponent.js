import React, { useEffect, useState } from "react";
import { View, StyleSheet } from "react-native";
import { ListItem, Avatar, Icon, Text } from 'react-native-elements';
import { update_product } from "../actions";
import image from '../common/imageCommon'// Only if no expo
import ModalAuction from './ModalAuctionComponent'
export default function AuctionComponent(props) {
    let listdata = []
    const [modalVisible, setModalVisible] = useState(false);
    const [itemProduct, setItemProduct] = useState({})
    const openModal = (item) => {
        setItemProduct(item)
        setModalVisible(!modalVisible)
    }
    console.log("peops.uer",props.user.data);
    const update_item=()=>{
          let data={
              ...itemProduct,
              customer:props.user.data._id,
              status:true
          }
          props.update_product({id:itemProduct._id,data:data})
    }
    if (props.product.product_all.data) {
        listdata = props.product.product_all.data.map((item, key) => {
            return (
                <ListItem style={styles.list_item} key={key} bottomDivider>
                    <Avatar size="medium" source={image[item.avata]} />
                    <ListItem.Content>
                        <ListItem.Title style={styles.text_but}>{item.name}</ListItem.Title>
                        <ListItem.Subtitle style={styles.text_but}>{item.price}$</ListItem.Subtitle>

                    </ListItem.Content>
                    <Icon
                        name='chevron-forward-outline'
                        type='ionicon'
                        color='pink'
                        onPress={() => { openModal(item) }}
                    />
                </ListItem>
            )
        })
    }
    return (
        <View>
            <View style={styles.container}>
                <View style={styles.buttonUser}>
                    <Text style={styles.text_but}>Sản phẩm đang đấu giá</Text>
                </View>
            </View>
            {listdata}
            <ModalAuction
                {...props}
                modalVisible={modalVisible}
                setModalVisible={setModalVisible}
                item={itemProduct}
                update_item={update_item}
                
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    text_but: {
        fontStyle: "italic",
    },
    list_item: {
        margin: 3
    },
    buttonUser: {
        margin: 7,
        width: '60%',
        height: 45,
        borderRadius: 25,
        borderWidth: 2,
        borderColor: 'pink',
        alignItems: 'center',
        paddingTop: 10,

    },
})