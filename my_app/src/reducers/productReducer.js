import * as types from '../common/index';
const DEFAULT_STATE = {
    product_all: [],
    product_author: [],
    product_customer: [],
    dataFetched: false,
    isFetching: false,
    error: false,
    errorMessage: null,
};

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.CREATE_PRODUCT_REQ:
            return {
                ...state,
                isFetching: true
            };
        case types.CREATE_PRODUCT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage: null
            };
        case types.CREATE_PRODUCT_FAIL:
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            };
        case types.GET_PRODUCT_REQ:
            return {
                ...state,
                isFetching: true
            };
        case types.GET_PRODUCT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage: null,
                product_all: action.payload
            };
        case types.GET_PRODUCT_FAIL:
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            };
        case types.GET_PRODUCT_BY_AUTHOR_REQ:
            return {
                ...state,
                isFetching: true
            };
        case types.GET_PRODUCT_BY_AUTHOR_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage: null,
                product_author: action.payload
            };
        case types.GET_PRODUCT_BY_AUTHOR_FAIL:
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            };
        case types.GET_PRODUCT_BY_CUSTOMER_REQ:
            return {
                ...state,
                isFetching: true
            };
        case types.GET_PRODUCT_BY_CUSTOMER_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage: null,
                product_customer: action.payload
            };
        case types.GET_PRODUCT_BY_CUSTOMER_FAIL:
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            };
        case types.UPDATE_PRODUCT_REQ:
            return {
                ...state,
                isFetching: true
            };
        case types.UPDATE_PRODUCT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage: null,
            };
        case types.UPDATE_PRODUCT_FAIL:
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            };
        default:
            return state;
    }
};
