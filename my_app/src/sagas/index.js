import { all } from 'redux-saga/effects';
import {UserSaga} from './userSaga'
import {ProductSaga} from './productSaga'
function* rootSaga() {
  yield all([
    ...UserSaga,
    ...ProductSaga
  ]);
}
export default rootSaga;
