import { put, takeEvery } from 'redux-saga/effects';
import * as types from '../common'
import callApi from '../fetchAPI/index'

function* login(action) {
    try {
        let res = yield callApi(`/login`, action.payload, 'POST')
        yield put({
            type: types.LOGIN_SUCCESS,
            payload: res
        })
    } catch (error) {
        yield put({
            type: types.LOGIN_FAIL,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
function* register(action) {
    try {
        let res = yield callApi(`/user`, action.payload, 'POST')
        yield put({
            type: types.REGISTER_SUCCESS,
            payload: res
        })
    } catch (error) {
        yield put({
            type: types.REGISTER_FAIL,
            payload: {
                errorMessage: error.message
            }
        })
    }
}

export const UserSaga = [
    takeEvery(types.LOGIN_REQ, login),
    takeEvery(types.REGISTER_REQ, register)
];