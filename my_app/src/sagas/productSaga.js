import { put, takeEvery } from 'redux-saga/effects';
import { get_product_by_author, get_product_by_customer } from '../actions';
import * as types from '../common'
import callApi from '../fetchAPI/index'

function* create_product(action) {
    try {
        let res = yield callApi(`/product`, action.payload, 'POST')
        yield put({
            type: types.CREATE_PRODUCT_SUCCESS,
            payload: res
        })
        yield put({
            type: types.GET_PRODUCT_BY_AUTHOR_REQ,
            payload: action.payload.author
        })
        yield put({
            type: types.GET_PRODUCT_REQ,
            payload: action.payload.author
        })
    } catch (error) {
        yield put({
            type: types.CREATE_PRODUCT_FAIL,
            payload: {
                errorMessage: error.message
            }
        })
    }
}

function* get_product_from_author(action) {
    try {
      
        let res = yield callApi(`/product/author/${action.payload}`, {}, 'GET')
  
        yield put({
            type: types.GET_PRODUCT_BY_AUTHOR_SUCCESS,
            payload: res
        })

    } catch (error) {
        yield put({
            type: types.GET_PRODUCT_BY_AUTHOR_FAIL,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
function* get_product_from_customer(action) {
    try {
 
        let res = yield callApi(`/product/customer/${action.payload}`, {}, 'GET')
  
        yield put({
            type: types.GET_PRODUCT_BY_CUSTOMER_SUCCESS,
            payload: res
        })

    } catch (error) {
        yield put({
            type: types.GET_PRODUCT_BY_CUSTOMER_FAIL,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
function* get_product(action) {
    try {
        let res = yield callApi(`/product`, {}, 'GET')
        yield put({
            type: types.GET_PRODUCT_SUCCESS,
            payload: res
        })

    } catch (error) {
        yield put({
            type: types.GET_PRODUCT_FAIL,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
function* update_product(action) {
    try {
        console.log('action',action.payload);
        let res = yield callApi(`/product/${action.payload.id}`, action.payload.data, 'PUT')
        yield put({
            type: types.UPDATE_PRODUCT_SUCCESS,
            payload: res
        })
        yield put({
            type: types.GET_PRODUCT_REQ,
            payload: res
        })
        yield put({
            type: types.GET_PRODUCT_BY_CUSTOMER_REQ,
            payload: action.payload.data.customer
        })

    } catch (error) {
        yield put({
            type: types.UPDATE_PRODUCT_SUCCESS,
            payload: {
                errorMessage: error.message
            }
        })
    }
}
export const ProductSaga = [
    takeEvery(types.CREATE_PRODUCT_REQ, create_product),
    takeEvery(types.GET_PRODUCT_BY_AUTHOR_REQ, get_product_from_author),
    takeEvery(types.GET_PRODUCT_BY_CUSTOMER_REQ, get_product_from_customer),
    takeEvery(types.GET_PRODUCT_REQ, get_product),
    takeEvery(types.UPDATE_PRODUCT_REQ,update_product)
];