import * as types from '../common/index'
export function login(payload){
    return({
        type:types.LOGIN_REQ,
        payload
    })
};
export function register(payload){
    return({
        type:types.REGISTER_REQ,
        payload
    })
};
export function create_product(payload){
    return({
        type:types.CREATE_PRODUCT_REQ,
        payload
    })
};
export function get_product(payload){
    return({
        type:types.GET_PRODUCT_REQ,
        payload
    })
}
export function get_product_by_author(payload){
    return({
        type:types.GET_PRODUCT_BY_AUTHOR_REQ,
        payload
    })
};
export function get_product_by_customer(payload){
    return({
        type:types.GET_PRODUCT_BY_CUSTOMER_REQ,
        payload
    })
};
export function update_product(payload) {
    return({
        type:types.UPDATE_PRODUCT_REQ,
        payload
    })
}