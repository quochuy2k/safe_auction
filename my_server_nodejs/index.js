const express = require('express'),
    app = express(),
    port = process.env.PORT || 4000,
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const server = http.createServer(app);
const cors = require('cors');
const { Server } = require("socket.io");
const io = new Server(server);
const {socket} = require('./src/socket/index')
mongoose.connect('mongodb://localhost:27017/safeinfor', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => {
    console.log("Connected!");
}).catch((err) => {
    console.log(err);
});
socket(io)
app.use(bodyParser.json({limit: '500mb'}));
app.use(bodyParser.urlencoded({limit: '500mb', extended: true}));

app.use(cors({ origin: '*' }))
var router = require('./src/route/index');
router(app);
app.use(function (req, res) {
    res.status(404).send({ url: req.originalUrl + 'not found' });
});
server.listen(port, () => {
    console.log('listening on *:',port);
  });