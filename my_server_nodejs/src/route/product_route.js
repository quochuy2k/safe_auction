const express = require('express')
const {
    create_new_product,
    delete_product,
    get_all_product,
    update_product,
    get_product_by_author,
    get_product_by_customer,
    get_product_by_id
} = require('../controller/product_controller')
const router = express.Router();
router.route('/product')
    .get(get_all_product)
    .post(create_new_product)
router.route('/product/:id')
    .delete(delete_product)
    .put(update_product)
    .get(get_product_by_id)
router.route('/product/author/:idUser')
    .get(get_product_by_author)
router.route('/product/customer/:idUser')
    .get(get_product_by_customer)
module.exports = router