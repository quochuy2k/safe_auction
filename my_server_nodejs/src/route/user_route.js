const express = require('express')
const {
    register,
    login
} = require('../controller/user_controller');

const router = express.Router();
router.route('/user')
    .post(register)
router.route('/login')
    .post(login)

module.exports = router