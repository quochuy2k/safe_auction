const userRoute = require('./user_route')
const productRoute = require('./product_route')

module.exports=(app)=>{
    app.use('/',[
        userRoute,
        productRoute,
 
    ])
}