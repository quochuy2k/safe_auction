const { Socket } = require('socket.io')
var client = {}
var price_max = {}
var time = {};

exports.socket = (io) => {
    io.on('connection', (socket) => {
        socket.on("join", (id) => {
            if (!time[id]) {
                var d = new Date();
                var n = d.getSeconds()+d.getMinutes()*60+d.getHours()*3600 + 60;
                time[id] = n
                socket.emit('time', time[id])
            }
            else {
                socket.emit('time', time[id])
            }

        });
        console.log(socket.id, " connected");
        socket.on("disconnect", (reason) => {
            console.log(`disconnect ${socket.id} due to ${reason}`);
        });
     
        socket.on('price', (price) => {
            if (client[price.product]) {
                if (price.price > price_max[price.product]) {
                    price_max[price.product] = price.price
                    client[price.product] = socket.id
                }
            }
            else {
                client[price.product] = socket.id
                price_max[price.product] = price.price
            }

            console.log("price....", client);
        });
        socket.on("result", (product_id) => {
            socket.emit("reponse_result", client[product_id])
        });

    });


}