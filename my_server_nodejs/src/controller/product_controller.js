const Product = require('../model/product')
function custom_properties(data) {
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    let dateTime = date + ' ' + time;
    let key = Math.floor(Math.random() * 10000000) + 1;
    let avatar = Math.floor(Math.random() * 101) ;
    let res = {
        ...data,
        avata: avatar,
        time_create: dateTime,
        key: key,
    }
    return res
}
exports.create_new_product = async (req, res) => {
    try {
        let data = await Product.create(custom_properties(req.body))
        res.status(200).json({ status: true, data: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: false, data: [] })
    }
}
exports.delete_product = async (req, res) => {
    try {
        let data = await Product.findByIdAndDelete(req.params.id)
        res.status(200).json({ status: true, data: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: false, data: [] })
    }
}
exports.update_product = async (req, res) => {
    try {
        let data = await Product.findByIdAndUpdate({ _id: req.params.id }, req.body)
        res.status(200).json({ status: true, data: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: false, data: [] })
    }
}
exports.get_all_product = async (req, res) => {
    try {
        let data = await Product.find({status:false})
        res.status(200).json({ status: true, data: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: false, data: [] })
    }
}
exports.get_product_by_author = async (req, res) => {
    try {
        let data = await Product.find({ author: req.params.idUser })
        res.status(200).json({ status: true, data: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: false, data: [] })
    }
}
exports.get_product_by_customer = async (req, res) => {
    try {
        let data = await Product.find({ customer: req.params.idUser })
        res.status(200).json({ status: true, data: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: false, data: [] })
    }
}
exports.get_product_by_id = async (req, res) => {
    try {
        let data = await Product.findById(req.params.id)
        res.status(200).json({ status: true, data: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: false, data: [] })
    }
}