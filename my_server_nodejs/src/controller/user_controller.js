const User = require('../model/user')
const md5 = require('md5');
exports.register = async (req, res) => {
    try {
        let password = md5(req.body.password)
        let user = {
            ...req.body,
            password: password
        }
        let data = await User.create(user)
        res.status(200).json({ status: true, data: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: false, data: [] })
    }
}
exports.login = async(req,res)=>{
    try {
        let pass = md5(req.body.password)
        let data = await User.findOne({user_name:req.body.user_name,password:pass});
        res.status(200).json({ status: true, data: data })
    } catch (error) {
        console.log(error);
        res.status(500).json({ status: false, data: [] })
    }
}
