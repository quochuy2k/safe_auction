const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const Schema = mongoose.Schema;
const product = new Schema({
    name: {
        type: String,
        require: true,
        unique:true
    },
    price: {
        type: Number,
        require: true
    },
    description:{
        type:String,
        require:true
    },
    time_life:{
        type: Number,
        require:true
    },
    time_create:{
        type:String,
        require:true
    },
    avata:{
        type:Number,
        require:true
    },
    author:{
        type:Schema.Types.ObjectId,
        ref:"user",
        require:true
    },
    customer:{
        type:Schema.Types.ObjectId,
        ref:"user",
        require:false
    },
    status:{
        type:Boolean,
        default:false
    },
    key: {
        type: Number,
        require: true
    }
})
product.plugin(mongoosePaginate)
module.exports=mongoose.model('product',product);